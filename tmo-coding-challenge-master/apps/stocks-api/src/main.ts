/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';

const request = require('request');

const init = async () => {
    const server = new Server({
        port: 3333,
      host: 'localhost'
    });
 server.route({
           method: 'GET',
           path: '/beta/stock/{symbol}/chart/{timeperiod}',
           handler: async (req, reply) => {
                 const symbol = req.params.symbol;
                 const timePeriod = req.params.timeperiod;
                 const token='Tpk_6f065baf291f48a7b768fd32d071a316';
               
                const response = server.methods.getdata(symbol,timePeriod,token); 
              
                 return response;
             
            }
        });


        const getdata = function (symbol, timePeriod, token) {
            const url = 'https://sandbox.iexapis.com/beta/stock/' + symbol + '/chart/' + timePeriod + '?token=' + token;
             return new Promise((resolve, reject) => {
                request(url, (error, response, body) => {
                  if (response && response['statusCode'] === 200) {
                    resolve(body);
                  } 
                });
            });
        };

       

      server.method('getdata', getdata, {
        cache: {
            expiresIn: 10*100000,
            generateTimeout: 2000000
        },
        generateKey: (symbol, timePeriod) => symbol + '_' + timePeriod
    });
       
       
  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
